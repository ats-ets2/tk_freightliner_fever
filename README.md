# README #

Mod for ATS Stock FreightLiner Trucks

* extend fuel capacity on Short/Mid/Long 6x4 Chassis
  * Short 6x4: 220 Gallons instead of 160
  * Mid/Long 6x4: 300 Gallons instead of 220 (mid) / 240 (long)
* 13 Gear Transmission with Retarder: Differential Ratio from 3.55 to 3.25 to match
  the differential ratio of the mostly equivalent 18 gear transmission.
* 18 Gear Transmission with Retarder: Add two more Versions with 3.08 and 3.55
  differential ratio. 3.55 for use with heavy loads.
* Those 4 13 and 18 Gear Transmissions: Set Gear Names similar to Snoman´s GearboxMOD
* Quick Jobs: Replace all "other" trucks (except Western Star 49x) with Freighliner
  ie.: no more Kenworth, Mack, Peterbilt, International Trucks in Quick Jobs.
  FreightLiner Cascadia and Western Star 49x only.


### What if I don´t want all of it? ###

* unzip and remove or modify the files you don´t want/need, especially the `def/.../transmission` or the `def/truck_company` folders
* split into multiple MODs


### Pro-Tip ###
* Instead of buying a different transmission for a different differential ratio, just change the settings in the transmission file. (needs the extracted MOD)


### How do I set it up? ###

* Download Zip
* Create Folder `<your ATS-Directory>/mods/tk_freightliner_fever`
* extract this archive into the newly created folder
* Activate in ATS Mod Manager
* Recommended: Use with new or copied profile.
